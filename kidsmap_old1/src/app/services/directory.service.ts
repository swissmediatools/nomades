import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AngularFirestore } from '@angular/fire/firestore';
import { Directoryitem } from '../models/Directoryitem';

@Injectable({
  providedIn: 'root'
})
export class DirectoryService {

  collectionName = 'restaurant';

  constructor( private afs: AngularFirestore ) {

   }
}
