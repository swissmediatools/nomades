export interface Directorycategory {
  id?: string;
  parent: string;
  name: string;
  slug: string;
  created: number;
  updated: number;
}
