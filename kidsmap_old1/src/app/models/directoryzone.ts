export interface Directoryzone {
  id?: string;
  parent: string;
  name: string;
  slug: string;
  lat: number;
  lng: number;
  created: number;
  updated: number;
}
