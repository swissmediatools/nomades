export interface Directoryitem {
  id?: string;
  name: string;
  slug: string;
  lat: number;
  lng: number;
  description: string;
  created: number;
  updated: number;
}
