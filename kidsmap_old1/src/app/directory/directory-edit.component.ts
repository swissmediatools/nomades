import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DirectoryService } from '../services/directory.service';

@Component({
  selector: 'app-directory-edit',
  templateUrl: './directory-edit.component.html',
  styleUrls: ['./directory-edit.component.scss']
})
export class DirectoryEditComponent implements OnInit {

  public directoryItemForm: FormGroup;

  constructor(private fb: FormBuilder, private ds: DirectoryService) { }

  ngOnInit(): void {
  }

}
