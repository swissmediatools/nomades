import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//  Import de FORMULAIRES
import { ReactiveFormsModule } from '@angular/forms'

//  Import de FIREBASE
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';

//  Import de ANGULAR MATERIAL
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTableModule } from '@angular/material/table';

//  Import des composants
import { FooterComponent } from './layout/footer.component';
import { HeaderComponent } from './layout/header.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { DirectoryListComponent } from './directory/directory-list.component';
import { DirectoryItemComponent } from './directory/directory-item.component';
import { DirectoryEditComponent } from './directory/directory-edit.component';
import { DirectoryComponent } from './directory/directory.component';


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    WelcomeComponent,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatTableModule,
    DirectoryListComponent,
    DirectoryItemComponent,
    DirectoryEditComponent,
    DirectoryComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
