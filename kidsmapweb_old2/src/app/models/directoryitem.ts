export interface DirectoryItem {
  id?: string;
  name: string;
  slug: string;
  descript: string;
  createdAt: number;
  updatedat: number;
}
