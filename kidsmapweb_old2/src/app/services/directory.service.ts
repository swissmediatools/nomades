import { Injectable } from '@angular/core';
import { Directoryitem } from '../models/directoryitem';
import { AngularFirestore } from '@angular/fire/firestore';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map,tap } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class DirectoryService {

  collectionName = 'items';

  private _directoryitems: BehaviorSubject<Directoryitem[]> = new BehaviorSubject<Directoryitem[]>([]);
  private directoryitems$: Observable<Directoryitem[]> = this._directoryitems.asObservable();

  constructor(private afs: AngularFirestore) { }

  saveItem(item)
  {
    console.log('Save Item Values : ',item);
  }

  readItems()
  {
    return this.afs.collection<Directoryitem>(this.collectionName);
  }

  public getItemById$(id: String) : Observable<Directoryitem>{
    console.log('id : ',id);
    return this.directoryitems$.pipe(
      map(directoryitems => directoryitems.find(directoryitem => directoryitem.id === id))
    )
  }

}
