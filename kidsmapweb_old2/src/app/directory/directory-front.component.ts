import { Component, OnInit } from '@angular/core';
import { DirectoryService } from '../services/directory.service';
import { Observable } from 'rxjs';
import { AngularFirestoreCollection } from '@angular/fire/firestore';
import { Directoryitem } from '../models/directoryitem';
import { Router } from '@angular/router';


@Component({
  selector: 'app-directory-front',
  templateUrl: './directory-front.component.html',
  styleUrls: ['./directory-front.component.scss']
})
export class DirectoryFrontComponent implements OnInit {

  directoryitemsCollection: AngularFirestoreCollection<Directoryitem>;
  directoryitems$: Observable<Directoryitem[]>;

  constructor(private ds: DirectoryService, private router: Router) { }

  async ngOnInit() {
    this.directoryitemsCollection = await this.ds.readItems();
    this.directoryitems$ = this.directoryitemsCollection.valueChanges({idField: 'id'});
  }

  async editDirectoryItem(itemkey)
  {
    console.log('Edit Directory Item ',itemkey);
    this.router.navigate([`/directory/${itemkey}/edit`]);

  }

}
