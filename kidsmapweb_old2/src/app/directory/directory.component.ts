import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directory',
  template: '<router-outlet></router-outlet>'
})
export class DirectoryComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
