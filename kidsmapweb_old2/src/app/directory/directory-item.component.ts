import { Component, OnInit } from '@angular/core';
import { Directoryitem } from '../models/directoryitem';
import { DirectoryService } from '../services/directory.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { map, filter, switchMap } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';


@Component({
  selector: 'app-directory-item',
  templateUrl: './directory-item.component.html',
  styleUrls: ['./directory-item.component.scss']
})
export class DirectoryItemComponent implements OnInit {

  private itemid: String;

  constructor( private route: ActivatedRoute ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.itemid = params['id'];
    });
    console.log('thisroute => ',this.route);
  }

}
