import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectoryFrontComponent } from './directory-front.component';

describe('DirectoryFrontComponent', () => {
  let component: DirectoryFrontComponent;
  let fixture: ComponentFixture<DirectoryFrontComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DirectoryFrontComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectoryFrontComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
