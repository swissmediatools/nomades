import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DirectoryItem } from '../models/directoryitem';
import { DirectoryService } from '../services/directory.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { map, filter, switchMap } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-directory-edit',
  templateUrl: './directory-edit.component.html',
  styleUrls: ['./directory-edit.component.scss']
})
export class DirectoryEditComponent implements OnInit {

  public directoryItemEditForm: FormGroup;
  private directoryItemSubscription: Subscription;
  private itemid$: String;

  constructor( private route: ActivatedRoute, private fb: FormBuilder, private ds: DirectoryService ) { }

  ngOnInit(): void {
    this.directoryItemEditForm = this.fb.group({
      id: [null],
      name: ['',[Validators.required, Validators.minLength(3)]]
    });
    console.log('thisroute => ',this.route);
    let itemid$: Observable<String> = this.route.paramMap.pipe(
      map(params => params.get('id')),
      filter(id => id !== null),
      map(id => String(id))
    )

    this.directoryItemSubscription = itemid$.pipe(
      switchMap(id => this.ds.getItemById$(id)),
      filter(item => item instanceof DirectoryItem)
    ).subscribe(item => this.directoryItemEditForm.setValue(item))
  }




  public saveDirectoryItem(): void {
    /*
    let data: Directoryitem = this.directoryItemEditForm.value;
    console.log('Submitted => ',data);
    */
  }

}
