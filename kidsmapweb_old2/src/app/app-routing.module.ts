import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { DirectoryComponent } from './directory/directory.component';
import { DirectoryFrontComponent } from './directory/directory-front.component';
import { DirectoryItemComponent } from './directory/directory-item.component';
import { DirectoryEditComponent } from './directory/directory-edit.component';

const routes: Routes = [
  { path: '', redirectTo: '/welcome', pathMatch: 'full' },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'directory', component: DirectoryComponent, children: [
      { path: '', component: DirectoryFrontComponent },
      { path: ':id', component: DirectoryItemComponent },
      { path: ':id/edit', component: DirectoryEditComponent },
      { path: 'new', component: DirectoryItemComponent }
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
