// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDABkUpTpZMh67HRxGs0qEnnXDB2907Lck",
    authDomain: "kidsmap-16893.firebaseapp.com",
    databaseURL: "https://kidsmap-16893.firebaseio.com",
    projectId: "kidsmap-16893",
    storageBucket: "kidsmap-16893.appspot.com",
    messagingSenderId: "548455845983",
    appId: "1:548455845983:web:2e37903387f515c2e30bea",
    measurementId: "G-2Z1PQ71DS9"
  },
  weather: {
    
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
