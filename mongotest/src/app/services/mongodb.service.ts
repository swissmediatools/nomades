import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MongodbService {

  public listitems;
  private api: string = '/api';
  private httpOptions: object = {
    headers: new HttpHeaders({
    }
    )
  }

  public items;

  constructor( public http: HttpClient ) { }

  getAllItems(): Observable<any> {
    return this.http.get(`${this.api}/items`);
  }

  listItems(): any
  {
    this.http.get(`${this.api}/items`,this.httpOptions).subscribe( getted => {
      console.log('Getted => ',getted);
      this.items = getted;
      return getted;
    } )
  }

}
