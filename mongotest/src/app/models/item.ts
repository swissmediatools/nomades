export interface Item {
    id?: string;
    name: string;
    created: number;
    updated: number;
}
