import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MongodbService } from '../services/mongodb.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor( private mds: MongodbService ) { }
  public items;

  ngOnInit(): void {
    this.retrieveItems();
  }

  async getItems() {
    return await this.mds.listItems();
  }

  retrieveItems(): void {
    this.mds.getAllItems()
      .subscribe(
        data => {
          this.items = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

}
